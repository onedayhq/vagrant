# A Vagrant VM Sandbox for Development

 This will create a sandbox VM in which will have all the tools required
 to get started with Oneday DevOps. This is a work in progress.

The ansible playbook will configure a ubuntu server to use as working sandbox for the environments. It's my personal preference not to clutter
the MAC laptop and use a VM.

It will add the following packages. Some may not be used at this time.

* ansible
* docker
* golang
* VPN openconnect
* pass (For passsord command line management)
* zsh and powerlevel10k theme 
* Keybase group password management
* direnv
* serverless utilities
  
Please see the README.md in the subdirectories and futher user configuration outside of the inital vagrant up --provision.

# How to start
Have vagrant and virtualbox installed. If not installed this won't work.
See <https://www.virtualbox.org> and <https://www.vagrantup.com>


Clone this repo on your mac then:

<pre>
# cd sandbox
# vagrant up
# vagrant ssh
</pre>

The vagrant up should auto-provision and install the tools on first run.

# Add fonts to your iterm2/terminal - A must have  to make your Mac and VM pretty

Setting up powerlevel10 theme will give you really nice git prompts. You will want to install the fonts in iterm2.

See 
https://dev.to/abdfnx/oh-my-zsh-powerlevel10k-cool-terminal-1no0

On setting up powerlevel10k with oh-my-zsh on the mac, It will auto-download the fonts and setup your iterm2 preferences. It's pretty easy, and even if you do not want to use the VM, having this setup is nice.

# Changing the Default theme for VM oh-my-zsh
The default theme for the VM is gnzh. You can also use powerlevel10k for the VM by setting:

<pre>
ZSH_THEME="powerlevel10k/powerlevel10k"
</pre>

in the .zshrc file. See sandbox/src/misc for example files. On next
login it should go through configuration to your taste. You can
also copy the example files over, to use the same theme I use.

You can always reconfigure with p10k config if you want to customize


